import { createRouter, createWebHistory } from 'vue-router'
import HomeView from '../views/HomeView.vue'
import ContainerView from '../views/ContainerView.vue';



const vueRouterConfig={
  //history:前端路由的模式，默认history模式， 修改成hash模式createWebHashHistory
  history: createWebHistory(import.meta.env.BASE_URL),
  //路由表:
  routes: [
    {
      path: '/',
      //命名路由
      name: 'home',
      component: HomeView
    },
    {
      path: '/about',
      name: 'about',
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import('../views/AboutView.vue')
    },
    {
      path:'/contain',
      name:'contain',
      component: ContainerView,
      //路由嵌套
      children:[
        {
          path:'/contain/hello',
          name:'hello',
          component:() => import('../views/HelloView.vue'),
          meta: { transition: 'slide-right' }
        },
        {
          path:'/contain/home',
          name:'chome',
          component:HomeView,
          meta: { transition: 'slide-right' }
        },
        {
          path:'/contain/about',
          name:'cabout',
          component:() => import('../views/AboutView.vue'),
          meta: { transition: 'slide-left' }
        },
      ]
    }
  ]
}


//创建了一个路由对象
const router = createRouter(vueRouterConfig)

export default router
