import { createApp } from 'vue'
import ElementPlus from 'element-plus'
import 'element-plus/dist/index.css'
import App from './App.vue'
import router from './router'

// import './assets/main.css'

const app = createApp(App)


app.use(ElementPlus)
// 注册路由
app.use(router)

app.mount('#app')
