import http from '@/utils/request.js'


// 所有产品数据 par = {page:1}  {params:{page:1}}
export const getProducts = async(par)=> {
    //res：包含了total，和需要的数据res.data
    let res = http.get('/v1/products',{params:par});
    return res;
};
// export const getProducts = ()=> http.get('/v1/products');

//某个产品的数据
export const getProductDetail = async(id)=> {
    let res = http.get(`/v1/products/${id}`).then(res=>res.data.data);
    return res;
};

export const getCategory = async() => {
    let res = http.get(`/v1/category`).then(res=>res.data.data);
    return res;
};