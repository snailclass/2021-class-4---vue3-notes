import { createRouter, createWebHistory } from 'vue-router'
import pinia from "@/stores/pinia.js"
import { useLoginStore } from '@/stores/login';




//state  getter action(isLogin())
// const loginStore = useLoginStore(pinia);






const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      alias: '/login',
      name: 'login',
      component: () => import('../views/LoginView.vue')
    },
    {
      path: '/index',
      name: 'index',
      component: () => import('../views/IndexView.vue'),
      meta: { title: '商品管理' },
      children: [
        {
          path: '/index/products',
          name: 'products',
          component: () => import('@/components/products/ProductsList.vue'),
          //表明需要登陆授权
          meta: { requireLogin: true, title: '商品列表' },
        },
        {
          path: '/index/sort',
          name: 'sort',
          component: () => import('@/components/products/ProductsSort.vue'),
          //表明需要登陆授权
          meta: { requireLogin: true, title: '商品分类' }
        },
        {
          path: '/index/product/:id',
          name: 'detail',
          component: () => import('@/components/products/ProductDetail.vue'),
          //表明需要登陆授权
          meta: { requireLogin: true, title: '商品详情' }
        }

      ]
    },

  ]
});

//全局导航守卫
/* router.beforeEach((to,from,next)=>{
  if(to.meta.requireLogin && !useLoginStore(pinia).isLogin()){
    //需要授权，但是没有登陆
    // next('/login')
    next({name:'login'})
    return;
  }else{
    //已经登陆，可以访问该页面
    next()
  }
}) */



export default router
