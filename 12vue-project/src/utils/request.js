//这是封装axios
import axios from 'axios';
//创建axios的实例
const http = axios.create({
    baseURL:"http://www.zdtech.top:3000",
    timeout:30000
})

export default http

//按需导出   可以导出多个：export const http

//默认导出 :只能导出一个 export default http