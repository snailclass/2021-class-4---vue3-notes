import { createRouter, createWebHistory } from 'vue-router'
import UserView from "@/views/UserView.vue"
import LoginView from "@/views/LoginView.vue"
import Login from "@/utils/Login.js"

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path:'/user/:id',
      name:'user',
      component: UserView,
      // 1.props为布尔值
      // props:true

      //2.props为一个对象
      // props:{name:'cp',age:10}

      // 3.props为一个函数
      props:(route)=>({name:'cp',age:10,id:route.params.id}),
      //重定向：将当前路由导向另一个路由
      // redirect:{name:'login'}
      meta:{requireLogin:true} //requireLogin：当前页面需要授权才能进
    },
    {
      path:'/',
      alias:'/login',
      name:'login',
      component:LoginView 
      //路由独享守卫
    }
  ]
})


//全局守卫
// to:即将要去的路由route对象(路由user中的route对象)
// from:从哪里来的路由对象(login中的route)
router.beforeEach((to,from,next)=>{
  console.log("to",to);
  console.log("from",from);

  //需要授权才能进入
  if(to.meta.requireLogin && !Login()){
    //需要授权，并且没有登陆， 不能进入
    next('/login');
  }
  else{
    //不需要授权
    //需要授权但是已经登陆了
    next()
  }
})





export default router
