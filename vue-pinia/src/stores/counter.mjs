import { ref, computed } from 'vue'
//defineStore(仓库id（唯一）,):创建一个管理库
import { defineStore } from 'pinia'
//state: 状态    
//getter: 计算属性 doubleCount
//action: methods

export const useAgeStore = defineStore('ageId',()=>{
  const age = ref(0)
  const name = ref('1')
  const arr = ref([1,2,3,5,6])

  //getter
  const doubleAge = computed(()=> age.value+1)

  const tripleAge = computed(()=> doubleAge.value*2)


  
  return{age,name,arr,doubleAge,tripleAge}
 });

//  export const useTestStore = defineStore('age1Id',()=>{
//  })


