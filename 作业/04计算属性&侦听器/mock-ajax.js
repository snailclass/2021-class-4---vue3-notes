function getPersonList(num) {
  return new Array(num).fill(0).map(() =>
    Mock.mock({
      name: Mock.Random.cname(),
      img: Mock.Random.image("100x100", Mock.Random.color()),
      des: Mock.Random.csentence(10, 30),
      "sex|1": ["m", "f"],
      id: Mock.Random.id(),
    })
  );
}

Mock.mock(/^\/user/, () => getPersonList(10));
