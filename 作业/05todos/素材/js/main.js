/* 
    头部：主要是添加待办事项
    按下回车时，将新待办事项传到父组件，让父组件执行添加操作
*/
const TodoHeader = {
    template: `
    <header class="header">
    <h1>todos</h1>
    <input
      autofocus="autofocus"
      autocomplete="off"
      placeholder="您的待办事项是?"
      class="new-todo"
      @keyup.enter="addTitle"
    />
    </header>
    `,
    emits:['add'],
    methods: {
        addTitle(event){
            this.$emit('add',event.target.value);
            //清空操作
            event.target.value = '';
        }
    },

};

/* 
    主要用于渲染，删除
    1.动态绑定样式：    completed:done为是否完成标志
                        editing：edit：是否处于编辑状态
    2.动态绑定属性：    :checked="done":是否完成标志

*/
const TodoItem = {
    template: `
    <li class="todo" :class="{completed:done, editing:edit}">
            <div class="view">
              <input type="checkbox" class="toggle" :checked="done" 
              @change="$emit('update:done',$event.target.checked)" />
              <label>{{title}}</label>
              <button class="destroy" @click.stop.self="$emit('delete')"></button>
            </div>
            <input type="text" class="edit" :value="title"
            @blur="$emit('update:edit',false)"
            @keyup.enter="onEdit"
            />
    </li>
    `,
    methods:{
        onEdit(event){
            this.$emit('update:edit',false);
            //传新的值
            console.log(event);
            this.$emit('update:title',event.target.value)
        }
    },
    props:["title","done","edit"],
    emits:["update:done","delete","update:edit","update:title"]
};


/* 
    主要用于footer的未完成，筛选功能等

*/

const TodoFooter = {
    template: `
    <footer class="footer">
        <span class="todo-count"> 有<strong>{{undo}}</strong>项未完成 </span>
        <ul class="filters">
          <li v-for="f in filterList"
          ><a href="#/all" class="selected" @click="$emit('update:filter',f.value)" >{{f.label}}</a></li>
        </ul>
        <button class="clear-completed" style="">清除已完成</button>
      </footer>
    `,
    props:["undo","filter"],
    data(){
        return{
            filterList:[
                {label:'全部',value:''},
                {label:'未完成',value:false},
                {label:'已完成',value:true},

            ]
        }
    },
};