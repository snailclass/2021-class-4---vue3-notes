//根组件以及子组件的导入
import MyButton from "./components/MyButton"


export default {
    data: function () {
        return {
            msg: "hello world!",
            num:0
        }
    },
    // template:
    template:`
    <my-button></my-button>
    `,
    //注册子组件
    components:{
        // "my-button":MyButton
        MyButton
    }
}