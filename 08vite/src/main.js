// vue中template不能自动转换成js
import { createApp } from 'vue'
// 导入根组件
import App from './App.vue'

//commonJS: const createApp = require('')
//ES6模块化规范： import ...  from vue


// 实例化并挂载相关实例
createApp(App).mount('#div');


//import导入