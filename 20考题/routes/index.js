const Router = require('koa-router');
const Blog = require('../models/blogModel');
const { Op } = require('sequelize');
const { withCtx } = require('vue');

const router = new Router();

//获取所有信息
function getAll() {
    //查找
    return Blog.findAll();
}

function getBlogbyTitle(title) {
    return Blog.findAll({
        where: {
            '标题': {
                [Op.like]: `%${title}%`
            }
        }
    })
}


router.get('/v1/all', async (ctx) => {
    const re = await getAll();
    console.log(111);
    ctx.body = {
        code: '200',
        status: 'Ok',
        data: re
    }

});

router.get('/v1/bytitle', async (ctx) => {
    //获取get发送的数据
    const { title } = ctx.query;
    const re = await getBlogbyTitle(title);
    console.log(111);
    ctx.body = {
        code: '200',
        status: 'Ok',
        data: re
    }

})


module.exports = router;