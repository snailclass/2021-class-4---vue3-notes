const { Sequelize, DataTypes, Op } = require("sequelize");

const config = {
    db: 'exam',
    usrname: 'sa',
    pwd: '123456',
    dialect: 'mssql'
}


// 连接配置
const sequelize = new Sequelize(config.db, config.usrname, config.pwd, {
    host: 'localhost',
    dialect: config.dialect
});



//定义模型 Blog表(标题，分类，作者，发布时间)
const Blog = sequelize.define('Blog', {

    // id 主键
    id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    //标题
    '标题': {
        type: DataTypes.STRING,
        allowNull: false,
    },
    '分类': DataTypes.STRING,
    '作者': DataTypes.STRING,
    '发布时间': {
        type: DataTypes.DATE,
        defaultValue:DataTypes.NOW
    }
}, {
    // 关闭事件戳
    timestamps: false
}
)



const blogArr = [
    { id: 1, '标题': '被讨厌的勇气', '分类': '心理', '作者': 'a' },
    { id: 2, '标题': '百年孤独', '分类': '叙事', '作者': 'b' },
    { id: 3, '标题': '挪威的森林', '分类': '叙事', '作者': 'c' },
    { id: 4, '标题': '忏悔录', '分类': '心理', '作者': 'd' },
    { id: 5, '标题': '柏拉图', '分类': '心理', '作者': 'e' }
]


    // 同步到数据库中
    ;; (
        async function () {
            // 同步到数据库中
            await sequelize.sync();
            // 修改表结构： await sequelize.sync({alter:true});

            // await Blog.bulkCreate(blogArr);
        }
    )()


    //将模型导出
module.exports = Blog;