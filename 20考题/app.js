const Koa = require("koa");
const path = require("path")
const koaStatic = require("koa-static");
const router = require("./routes/index");


const app = new Koa();

// 加载静态资源
app.use(koaStatic(__dirname+'/public'));

// 挂载路由
app.use(router.routes()).use(router.allowedMethods());

app.listen(3000,()=>{
    console.log('server is running on http://localhost:3000');
});